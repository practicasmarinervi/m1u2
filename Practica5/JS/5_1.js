



//Guardamos en un array los valores de las respuestas correctas
var respuestasCorrectas=new Array("c","a","c","c","c","b","a","b","d","b");
var tiempoLimite=0;
var tiempo;  //variable global

window.addEventListener("load",cargar);

function cargar(){
	document.querySelector("#inicio").addEventListener("click", reloj);
	}  //LLama a la funcion contador una vez que el usuario clica en iniciar test

function reloj(){
	//Tomamos fecha  y hora del sistema

	var  dia = new Date();
	var  hora = dia.getHours();
	var  minutos = dia.getMinutes();
	var  segundos = dia.getSeconds();
			
	 //Convertimos los números a dos dígitos

    if ((hora>=0) && (hora<=9)){
	 	 hora="0"+hora;}

    if ((minutos>=0) && (minutos<=9)){
	 	 minutos="0"+minutos;}
        
	if ((segundos>=0) && (segundos<=9)){
	 	 segundos="0"+segundos;}

	//La hora completa
	tiempo = hora+":"+minutos+":" +segundos
	
	document.querySelector("#test").style.display="block"; //Mostramos al usuario las preguntas
	document.querySelector("#reloj").style.display="block"; //Mostramos el reloj con la hora del sistema
	document.querySelector("#reloj").innerHTML = tiempo;
	document.querySelector("#inicio").style.display="none"; //Ocultamos el boton de iniciar test
	        

	//Con esta instruccion, establecemos el contador y va incrementando tiempo de segundo en segundo
	//
	//Es muy importante que el método a ejecutar que especifiquemos en
	//la función setTimeout lo pongamos entre comillas.
		tiempo=setTimeout("reloj()",1000);
	 	
		if (tiempoLimite>=60) {
			//Paramos el reloj
			clearTimeout(tiempo);
			var opciones=document.querySelectorAll("[type=radio]");
			for (var i = 0; i < opciones.length; i++) {
				opciones[i].disabled=true;
			}
			alert("Se acabo el Tiempo!!! Pulsa en corregir");

			} else{
		tiempoLimite+=1;
		}
		document.f.corregir.addEventListener("click", function(){
			//Paramos el reloj
	 	clearTimeout(tiempo);
		mostrar(respuestasCorrectas);
	});

	}
function mostrar(respuesta){
		
	// Se inicializan los contadores para las respuestas
	var aciertos=0;
	var fallos=0;
	var nulas=0;
		
	var solucion = document.querySelector("#solucion");
	document.querySelector("#test").style.display="none"; 
	document.querySelector("#solucion").style.display="block"; //Mostramos al usuario los resultados
	var resultados =""; //Creamos una variable tabla para ir metiendo resultados

	for (var i=0;i<respuesta.length;i++) {
		var botones = document.getElementsByName(i+1);
		for (var j = 0; j < botones.length; j++) {
			if (botones[j].checked){
				var valor=botones[j].value;
				if (valor==respuesta[i]) {
					resultados+=(i+1)+". Respuesta correcta: " + valor+ "<br>";
					aciertos++;
					break; 
				} else {
					resultados+=(i+1)+". Respuesta incorrecta:" + valor+ "<br>";
					fallos++;
					break;
				}
			} 
		}
       	
	}
	
	nulas=respuesta.length-(aciertos+fallos);
	resultados+="Resultado: Respuestas correctas: "+aciertos+"<br/>Respuestas incorrectas: "+ fallos+
	"<br/>Respuestas en blanco: "+ nulas+ "<br> Tu puntuacion total ha sido de: "+aciertos;
	solucion.innerHTML = resultados;
}

						
						
			