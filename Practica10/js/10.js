//precarga de imágenes
var imagenesBien = new Array();
var imagenesMal = new Array();
for (var i = 0; i < 3; i++) {
    imagenesBien[i] = new Image();
    imagenesBien[i].url = "url(./imgs/bien/" + (i + 1) + "bien.png)";
    imagenesMal[i] = new Image();
    imagenesMal[i].url = "url(./imgs/mal/" + (i + 1) + "mal.png)";
}

//cargar web                  
window.addEventListener("load", cargar);

//variables globales
var cajasL = new Array();
var lamina;
var errores;


function cargar() {
    //control de tiempo
    var barra = document.querySelector(".tiempo"); //fijamos el intervalo de tiempo
    barra.style.width = "100%";

    lamina = 0;
    errores = 0;
    cambiarLamina(lamina);
    //situar errores de primera lámina
    coordenadas(lamina);
}


// Hacemos disminuir la barra de tiempo
function disminuir() {
    var barra = document.querySelector(".tiempo");
    var ancho = parseInt(barra.style.width);
    ancho = ancho - 5;
    if (event.target.className == "errores") {

        if (ancho >= 0) {
            barra.style.width = ancho + "%";
        } else {

            //muchos errores y puedes volver a jugar     
            if (confirm("Fin del juego. Prueba de nuevo")) {
                cargar();
            } else {
                document.querySelector(".gameOver").style.display = "block";
            }
        }
    }
}

// recoger las coordenadas de los tic

function coordenadas(lamina) {

    var posiciones;

    switch (lamina) {
        case 0:
            posiciones = new Array([53, 156, 150, 90], [80, 300, 50, 65], [60, 187, 100, 64], [60, 170, 50, 24], [60, 115, 50, 70]);
            break;
        case 1:
            posiciones = new Array([43, 204, 35, 35], [45, 316, 50, 51], [-8, 155, 92, 39], [97, 175, 44, 58], [114, 127, 28, 40]);
            break;
        case 2:
            posiciones = new Array([140, 315, 20, 20], [170, 355, 20, 20], [62, 400, 20, 20], [138, 280, 25, 25], [57, 436, 25, 25]);
            break;
    }

    //poner coordenadas
    for (var c = 0; c < posiciones.length; c++) {
        do {

            var i = 0;
            cajasL[c].style.top = posiciones[c][i] + "px";
            i++;
            cajasL[c].style.left = posiciones[c][i] + "px";
            i++;
            cajasL[c].style.width = posiciones[c][i] + "px";
            i++;
            cajasL[c].style.height = posiciones[c][i] + "px";
            i++;

        } while (i < 4);

    }

}



//Realiza el marcado de los aciertos
function marcar(event) {
    //saber en qué lámina estamos
    var cajaError = event.target;

    //si no tiene tic, poner tic al encontrar error
    if (cajaError.style.backgroundImage == "none") {
        event.target.style.backgroundImage = "url(./imgs/tic.png)";
        errores++;
    }

    //Al encontrar los 5 errores, aumentar lámina, cambiar ésta
    // y sus coordenadas de errores
    // e inicializar de nuevo errores
    if (errores == 5) {
        if (lamina < 2) {
            lamina++;
            cambiarLamina(lamina);
            coordenadas(lamina);
            errores = 0;
        } else {
            clearInterval(reloj);
            alert("Enhorabuena Acabaste");
        }
    }
}

//Al encontrar los cinco errores cambia la imagen de fondo ya asignar click de nuevo
function cambiarLamina(num) {

    var laminasBien = document.querySelector(".fotos>div:first-child");
    //imagen a la izda
    var laminasMal = document.querySelector(".fotos> .errores");
    //imagen a la dcha
    laminasBien.style.backgroundImage = imagenesBien[lamina].url;
    laminasMal.style.backgroundImage = imagenesMal[lamina].url;

    cajasL = document.querySelectorAll(".errores>div"); //llegamos a los div de los errores

    document.querySelector(".errores").addEventListener("click", disminuir);

    for (var i = 0; i < cajasL.length; i++) {
        cajasL[i].addEventListener("click", marcar);
        cajasL[i].style.backgroundImage = "none";
    }
}