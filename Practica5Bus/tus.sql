-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-10-2017 a las 17:17:51
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tus`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paradas`
--

CREATE TABLE `paradas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `info` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `paradas`
--

INSERT INTO `paradas` (`id`, `titulo`, `foto`, `info`) VALUES
(0, 'valdecilla', 'imgs/valdecilla.png', 'Línea 11 Valdecilla - C/Alta'),
(1, 'calle alta (mercado)', 'imgs/calta.png', 'Línea 11 Valdecilla - C/Alta'),
(2, 'calle alta 109 A', 'imgs/valdecilla.png', 'Línea 11 Valdecilla - C/Alta'),
(3, 'calle alta 81', 'imgs/calta.png', 'Línea 11 Valdecilla - C/Alta'),
(4, 'calle alta 45', 'imgs/valdecilla.png', 'Línea 11 Valdecilla - C/Alta'),
(5, 'monte caloca', 'imgs/calta.png', 'Línea 11 Valdecilla - C/Alta'),
(6, 'jesús de monasterio 21', 'imgs/valdecilla.png', 'Línea 11 Valdecilla - C/Alta'),
(7, 'ayuntamiento', 'imgs/valdecilla.png', 'Línea 11 Valdecilla - C/Alta'),
(8, 'calle alta 28', 'imgs/calta.png', 'Línea 11 Valdecilla - C/Alta'),
(9, 'calle alta 46', 'imgs/valdecilla.png', 'Línea 11 Valdecilla - C/Alta'),
(10, 'calle alta 56', 'imgs/calta.png', 'Línea 11 Valdecilla - C/Alta'),
(11, 'calle alta 80', 'imgs/valcedilla.png', 'Línea 11 Valdecilla - C/Alta'),
(12, 'argentina 7', 'imgs/calta.png', 'Línea 11 Valdecilla - C/Alta'),
(13, 'plaza de toros', 'imgs/valdecilla.png', 'Línea 11 Valdecilla - C/Alta');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `paradas`
--
ALTER TABLE `paradas`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
