window.addEventListener("load", function(){

		var areas=document.querySelectorAll("area");
	for (var i = 0; i < areas.length; i++) {
		areas[i].addEventListener("click",mostrar);
	}
	
});


function crearModal(){
	var sal=document.createElement("div");
	var h1=document.createElement("h1");
	var cerrar=document.createElement("div");
	var imagen=document.createElement("img");
	var informacion=document.createElement("div");

	cerrar.appendChild(document.createTextNode("X"));
	sal.appendChild(cerrar);
	sal.appendChild(h1);
	sal.appendChild(imagen);
	sal.appendChild(informacion);
	sal.className="salida";
	cerrar.addEventListener("click", function(){
		document.querySelector("body").removeChild(sal);
	});
	return sal;
}




function mostrar(){

	var sal=crearModal();

	var num=event.target.getAttribute("data-id");
	var parada={
		titulo:datos[num].titulo.toUpperCase(),
		foto:datos[num].foto,
		info:datos[num].info
	}

	if(document.querySelector(".salida")!=null){
		document.querySelector("body").removeChild(document.querySelector(".salida"));
	}

	document.querySelector("body").appendChild(sal);
	document.querySelector(".salida h1").innerHTML=parada.titulo;
	document.querySelector(".salida img").src=parada.foto;
	document.querySelector(".salida div:last-of-type").innerHTML=parada.info;

}

