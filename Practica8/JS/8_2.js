
window.addEventListener("load",function(){

  //Con esta instruccion, establecemos el contador y va incrementando tiempo de segundo en segundo
  //
  //Es muy importante que el método a ejecutar que especifiquemos en
  //la función setInterval lo pongamos entre comillas.

  setInterval("reloj()",1000);
  }); 

function reloj(){
  //Tomamos fecha  y hora del sistema

  var  dia = new Date();
  var  hora = dia.getHours();
  var  minutos = dia.getMinutes();
  var  segundos = dia.getSeconds();
      
   //Convertimos los números a dos dígitos

    if ((hora>=0) && (hora<=9)){
     hora="0"+hora;}

    if ((minutos>=0) && (minutos<=9)){
     minutos="0"+minutos;}
        
  if ((segundos>=0) && (segundos<=9)){
     segundos="0"+segundos;}

  //La hora completa
  var tiempo = hora+":"+minutos+":" +segundos
  
     document.querySelector("#reloj").innerHTML = tiempo;
     

  
  }