 this.g = {};

 g.v = 1; //contador para saber cúantos fantasmas hay que colocar
 g.comida = 0; //verdes que te comes
 g.puntuación = 0;
 g.salir = true; // detiene el for de la colision

 //objeto comecocos
 g.pacoco = {
     x: 0,
     y: 0
 };

 //precargar de imágenes

 //fantasmas
 var verdes = new Image();
 verdes.src = "imgs/cocoVerde.png";
 var rojos = new Image();
 rojos.src = "imgs/cocoRojo.png";

 //comecocos
 var cocos = new Array();

 for (var c = 0; c < 4; c++) {
     cocos[c] = new Image();
     cocos[c].src = "imgs/" + [c] + ".png";
 }

 function dibujarCanvas() {
     g.canvas = document.getElementById('miCanvas');
     g.contexto = g.canvas.getContext('2d');
     g.buenos = new Array();
     g.malos = new Array();

     //clearTimeout(g.bucle);  Si quieres que no se acelere entre niveles, descomenta esta linea

     colocarFantasmas(g.v);
     f1();
     window.addEventListener("keydown", f2);
 }

 function f2() {
     //console.log(event.which);
     switch (event.which) {
         case 38:
             g.direccion = "arriba";

             break;
         case 40:
             g.direccion = "abajo";

             break;
         case 37:
             g.direccion = "izquierda";

             break;
         case 39:
             g.direccion = "derecha";

             break;
     }
 }

 function f1() {

     g.bucle = setTimeout(function() { window.requestAnimationFrame(f1) }, 5);
     //window.requestAnimationFrame(f1); 

     //hacer que rebote en los limites del canvas
     if (g.pacoco.y < 0) {
         g.direccion = "abajo";


     } else if (g.pacoco.y > 500) {
         g.direccion = "arriba";
     }

     if (g.pacoco.x < 0) {
         g.direccion = "derecha";
     } else if (g.pacoco.x > 800) {
         g.direccion = "izquierda";
     }

     limpiar();



     switch (g.direccion) {
         case "arriba":
             g.contexto.drawImage(cocos[3], g.pacoco.x, g.pacoco.y);
             g.pacoco.y -= 1;
             break;
         case "abajo":
             g.contexto.drawImage(cocos[1], g.pacoco.x, g.pacoco.y);
             g.pacoco.y += 1;
             break;
         case "izquierda":
             g.contexto.drawImage(cocos[2], g.pacoco.x, g.pacoco.y);
             g.pacoco.x -= 1;
             break;
         case "derecha":
             g.contexto.drawImage(cocos[0], g.pacoco.x, g.pacoco.y);
             g.pacoco.x += 1;
             break;
         default:
             g.contexto.drawImage(cocos[0], g.pacoco.x, g.pacoco.y);
             //g.contexto.drawImage(coco,200, 180);
             break;
     }

     if (g.salir) {


         for (var c = 0; c < g.v; c++) {
             g.contexto.drawImage(verdes, g.buenos[c].xV, g.buenos[c].yV);
             g.contexto.drawImage(rojos, g.malos[c].xR, g.malos[c].yR);
             colision(g.malos[c]);
             colision(g.buenos[c]);
         }
     } else {
         clearTimeout(g.bucle);
         window.removeEventListener("keydown", f2);
     }
 }

 /*
    Función que recibe como argumento el número de fantasmas de cada color que se deben 
    colocar en la pantalla de cada nivel
  */
 function colocarFantasmas(v) {

     for (var c = 0; c < v; c++) {
         var xV = Math.floor(Math.random() * 801);
         var yV = Math.floor(Math.random() * 501);
         g.buenos.push({ xV, yV });

         var xR = Math.floor(Math.random() * (801 - 0));
         var yR = Math.floor(Math.random() * (501 - 0));
         g.malos.push({ xR, yR });
     }

 }

 /*
     Función que recibe como argumento el objeto fantasma a analizar y comprueba si 
     las coordenadas de ese objeto coinciden con la posición del comecocos
      Si el fantasma es verde y coinciden posiciones:
         el fantasma desaparece
         suma uno a la puntuación
         aumenta g.v
      Si el fantasma es rojo
         GAME OVER
  */
 function colision(bicho) {
     if (((g.pacoco.x + 50 >= bicho.xR) && (g.pacoco.x < bicho.xR + 50)) && ((g.pacoco.y + 50 >= bicho.yR) && (g.pacoco.y <= bicho.yR + 55))) {
         g.salir = false; //para que pare la ejecucion del for
         looser();
     }

     if (((g.pacoco.x + 50 >= bicho.xV) && (g.pacoco.x < bicho.xV + 50)) && ((g.pacoco.y + 50 >= bicho.yV) && (g.pacoco.y <= bicho.yV + 55))) {


         g.comida++;
         bicho.xV = -60;
         bicho.yV = -60;
         if (g.v == g.comida) {
             g.v++;
             dibujarCanvas();
             g.comida = 0;
         }

         g.puntuación++;
         sumaPuntos();
     }

 }

 /* limpiar lienzo */
 function limpiar() {
     g.contexto.clearRect(0, 0, g.canvas.width, g.canvas.height);
     g.contexto.fillRect(0, 0, g.canvas.width, g.canvas.height);
 }

 function looser() {

     alert("GAME OVER");

     //recoger datos y enviar a server

     validarFormulario();
     
     document.f.submit();

 }

 function validarFormulario() {

     if (document.f.puntuacion.value == "") {
         document.f.puntuacion.value = 0;
     }
     document.f.nombre.value = document.f.nombre.value.toUpperCase();

     

 }

 function sumaPuntos() {
     document.querySelector("#puntos").value = g.puntuación + " puntos";
 }

 function listadoPuntos() {

     var ventana = document.querySelector(".marcador");

     ventana.style.display = "block";
     var titulo = document.createElement("h2");
     titulo.appendChild(document.createTextNode("PUNTUACIONES"));
     ventana.appendChild(titulo);
     ventana.appendChild(crearTabla(datos));
 }



 function crearTabla(arg) {

     var tabla = document.createElement("table");
     var filas = new Array();
     var celdas = new Array();
     var texto = new Array();


     for (var i = 0; i < arg.length; i++) {

         filas[i] = document.createElement("tr"); // creamos la fila

         texto[i] = document.createTextNode(arg[i].id); //creamos un elemento de texto
         celdas[i] = document.createElement("td"); // creamos las celdas
         celdas[i].appendChild(texto[i]); //hacemos que ese texto cuelgue de la celda
         filas[i].appendChild(celdas[i]); //hacemos que las celdas esten en las filas

         texto[i] = document.createTextNode(arg[i].nombre);
         celdas[i] = document.createElement("td");
         celdas[i].appendChild(texto[i]);
         filas[i].appendChild(celdas[i]);

         texto[i] = document.createTextNode(arg[i].puntuacion);
         celdas[i] = document.createElement("td");
         celdas[i].appendChild(texto[i]);
         filas[i].appendChild(celdas[i]);

         tabla.appendChild(filas[i]); // finalmente haremos que las filas esten contenidas en la tabla

     }
     return tabla;
 }

 window.addEventListener("load", function() {

     document.querySelector("#jugar").addEventListener("click", function() {

         var campoNombre = document.f.nombre;
         var p = prompt("Introduce tu nombre para guardar puntuacion");

         if (!p) {

             campoNombre.value = "desconocido";

         } else {

             campoNombre.value = p;

             campoNombre.readOnly = "true";
         }

         campoNombre.value = campoNombre.value.toUpperCase();

         dibujarCanvas();

     });

     document.querySelector("#puntua").addEventListener("click", listadoPuntos);

 });