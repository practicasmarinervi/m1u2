<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>Practica31</title>
    <?php include_once "libreria.php"; ?>
    <link rel="stylesheet" type="text/css" href="css/p31.css">
    <script type="text/javascript" src="js/p31.js"></script>
</head>

<body>
    <div class="contenedor">
        <div>
            <input type="button" name="JUGAR" id="jugar" value="JUGAR">
            <input type="button" name="PUNTUACION" id="puntua" value="PUNTUACION">
        </div>
        <canvas id="miCanvas" width="850" height="550" style="background:black">Su navegador no soporta Canvas.</canvas>

        <form action="almacenar.php" method="post" name="f">

        <div>
            <input type="text" id="puntos" name="puntuacion" readonly=>
            <input type="text" class="label" value="PUNTUACION" disabled="">
        </div>

        <div><input type="text" id="nom" name="nombre">
        <input type="text" class="label" value="NOMBRE" disabled="">
        </div>
        </form>
    </div>
    
    <div class="marcador"></div>
</body>

</html>