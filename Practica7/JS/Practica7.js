
//Precarga de imagenes
//
var imagenes=new Array();
for (var i = 0; i < 10; i++) {
   imagenes[i]=new Image();
   imagenes[i].src="imgs/"+(i+1)+".png";
}


window.addEventListener("load",carga);

this.g={}; //definimos un contenedor global para todas las variables

function carga(){
   g.tiempo=document.querySelector(".tiempo");
   g.tiempo.style.display="none";
   g.palabra=document.querySelector(".palabra");
   g.errores=document.querySelector(".errores");
   g.ahorcado=document.querySelector(".ahorcado");

   //Asignamos evento a nuevo juego
   //
   g.inicio=document.querySelector(".inicio");
   g.inicio.addEventListener("click",empieza);

   //ponemos a cero el contador de las partidas ganadas
   //
   g.exito=0;

   //LLegamos a la caja donde se va a mostrar resultados de los ganadores
   //
   g.marcador=document.querySelector(".marcador");
   g.marcador.style.display="none";

};



/*Iniciamos el tiempo de la partida*/


function empieza(){
   g.exito++;
   //mostramos display con reloj
   g.tiempo.style.display="block";
   //LLegamos a las cajas donde se muestra el reloj
   //
   g.s=document.querySelector(".segundos");
   g.p=document.querySelector(".puntos");
   g.m=document.querySelector(".minutos");

   //Iniciamos con un tiempo determinado de dos minutos
   //
   g.minutos=2;
   g.segundos=0;
   //ponemos en marcha el cronometro
   g.crono=setInterval(function(){crono()},1000);

   //Ahora instanciamos una nueva partida
   //
   var partida=new Jugada();

   //Asignamos evento de tecla pulsada a window
   document.addEventListener("keypress",tecla);
   g.palabra.innerHTML=partida.salida;

   //separamos caracteres
   g.caracteres=partida.palabra.split("");
   g.salidaCaracteres=partida.salida.split("");
   //inicializamos variables
  
   g.fallos=0;
   g.letra="";
   g.ahorcado.style.backgroundImage="";
   g.errores.innerHTML="";
   g.errores.style.backgruoundColor="";
};

/*comprobamos las teclas que se pulsan y las comparamos con la de la palabra oculta*/

function tecla(){
   //primero inicializamos fallos
   var fallo=1;
   //comprobamos tecla pulsada y la pasamos a mayuscula
   g.letra=String.fromCharCode(event.keyCode);
   g.letra=g.letra.toUpperCase();
   //recorremos la palabra oculta para ver si tiene esa letra
   for (var i = 0; i < g.caracteres.length; i++) {
      if(g.caracteres[i]==g.letra){
         g.salidaCaracteres[i]=g.letra;
         fallo=0;//no hay errores si la letra pulsada esta en la palabra oculta
      };
   };
   if(fallo){
      //muestra parte de ahorcado y suma uno a los errores
      dibujo();
      g.fallos++;
      g.errores.innerHTML=g.fallos;
   }else{//si la letra existe instanciamos de nuevo y mostramos letra
      var partida=new Jugada();
      partida.salida=g.salidaCaracteres.join("");
      g.palabra.innerHTML=partida.salida;
   };

   //Si adivinas la palabra oculta vas a ganadores
   if(g.salidaCaracteres.toString()==g.caracteres.toString()){
      ganar();
   };
   if(g.fallos==10){
      perder();
   };
   if(g.crono.minutos==0 && g.crono.segundos==0){
      //paramos el tiempo
      clearInterval(g.crono);
   };
};

/*Funcion para ganar*/
function ganar(){
   g.ahorcado.style.backgroundImage="url('imgs/ganador.png')";
   //como se ha ganado, quitamos el evento de teclado
   teclado();
   clearInterval(g.crono);
   //mostramos los ganadores del juego
   jugadores();
};

/*Funcion para perder*/
function perder(){
   g.errores.style.backgroundColor="red";
   g.errores.innerHTML="HAS PERDIDO";
   g.ahorcado.style.backgroundImage="url('imgs/bolaloser.jpg')";
   //como se ha perdido, quitamos el evento de teclado
   teclado();
   //paramos el tiempo
   clearInterval(g.crono);
};

/*Funcion para dibujar muñeco*/
function dibujo(){
   g.ahorcado.style.backgroundImage="url('"+imagenes[g.fallos].src+"')";
  };

/*Funcion que quita el evento de teclado*/
function teclado(){
   document.removeEventListener("keypress",tecla);
  };

/*Funcion mostrar jugadores, tiempo y puntuacion*/
function jugadores(){
   clearInterval(g.crono);
   //pregunta nombre, lo almacena en local y lo sube a marcador
  var nombre=prompt("Introduce tu nombre:");
  localStorage.setItem("nombre",nombre);
   //ahora almacenamos los datos en un array en el cual cada elemento
   //es un objeto que almacena nombre de jugador y tiempo
   var ganador=new Array();
   for(var c=0;c<g.exito;c++){
      ganador[c]=new Object();
      ganador[c].nombre=localStorage.getItem("nombre");
      ganador[c].tiempo=g.minutos+":"+g.segundos;
   };

   //y mostramos los ganadores
   g.marcador.style.display="block";
   g.marcador.innerHTML+="Ganador o Ganadora: "+
   ganador[g.exito-1].nombre+"- Tiempo: "+
   ganador[g.exito-1].tiempo+"<br>";
  };

  /*creamos la clase jugada*/

  var Jugada=function(){
   //con sus propiedades
   var sup=this;
   var palabrasOcultas;
   this.palabra;
   this.salida;
   //y sus metodos
   function palabraAleatoria(){
      sup.palabra=palabrasOcultas[Math.floor(Math.random()*palabrasOcultas.length)];
      return sup.palabra.toUpperCase();
   };
   function guiones(){
      sup.salida="-";
      return sup.salida=sup.salida.repeat(sup.palabra.length);
   };

   //este es elmetodo constructor
   function Jugada(){
      palabrasOcultas=new Array("informatica","programacion","automata","peregrinacion",
         "destartalado","estupido","concienzudo","desastroso","rizo","xilofono");
      sup.palabra=palabraAleatoria();
      sup.salida=guiones();
   };
   //llamamos a la clase
   Jugada();
  };

  /*Funcion para cronometrar*/

  function crono(){
   //restamos los segundos
   g.segundos-=1;
   //cuando estos lleguen a cero, los reiniciamos y restamos 1 minuto
   if(g.segundos<0){
      g.segundos=59;
      g.minutos-=1;
   };
   //introducimos hora en la caja del reloj
   g.s.innerHTML=cero(g.segundos);
   g.p.innerHTML=" : ";
   g.m.innerHTML=cero(g.minutos);
   //comprobar si minutos y segundos estan a cero
   if(g.minutos==0 && g.segundos==0){
      perder();
      g.errores.innerHTML+="<br> TIME OUT";
   };

  };

  /*Funcion para mostrar dos digitos en el reloj*/
  function cero(temp){
   if(temp<10){
      temp="0"+temp;
   };
   return temp;
  };