<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/main.css" type="text/css" />
	<title>Ramon Abramo</title>
	<?php include_once "libreria.php"; ?>
	<script type="text/javascript" src="JS/main.js"></script>

</head>
<body>
	<div id="wrap">
		<div id="logo">
			<h1><a href="#">Web de clase</a></h1>
		  <p>Realizada en el curso de 2017</p>
		</div>
		
		
		<div id="menu">
			<div id="menu-left"></div>
			<ul>
				<li><a class="current" href="#"><span>Inicio</span></a></li>
				<li><a href="#"><span>Mis aficiones</span></a></li>
				<li><a href="#"><span>Mis Peliculas</span></a></li>
				<li><a href="#"><span>Mis Libros</span></a></li>
				<li><a href="#"><span>Mis Juegos</span></a></li>
			</ul>
		</div>
		
		<div id="content-top"></div>
		<div id="content-middle">
			<div id="pitch">
				<h1><br /><span>Entra a disfrutar</span></h1>
				<p></p>
			</div>
		
			<div class="column">
				<h3></h3>
				<img src="images/thumb.gif" alt="Thumb" />
				<p></p>
				<p class="more"><a href="#">read more</a></p>
			</div>

			<div class="column">
				<h3></h3>
				<img src="images/thumb.gif" alt="Thumb" />
				<p></p>
				<p class="more"><a href="#">read more</a></p>
			</div>	
			
			<div class="column last">
				<h3></h3>
				<img src="images/thumb.gif" alt="Thumb" />
				<p></p>
				<p class="more"><a href="#">read more</a></p>
			</div>	
		
			<div class="clear"></div>
		</div>
		
		<div id="content-bottom"></div>
	
		<div id="footer">
			<p id="links">
				<a href="#">Web 1</a>
				<a href="#">Web 2</a>
				<a href="#">Web 3</a>
			</p>
		</div>
	</div>	
</body>
</html>