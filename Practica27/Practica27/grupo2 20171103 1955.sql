﻿-- Script was generated by Devart dbForge Studio for MySQL, Version 6.0.128.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 03/11/2017 19:55:33
-- Server version: 5.5.5-10.1.16-MariaDB
-- Client version: 4.1

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

-- 
-- Set default database
--
USE grupo2;

--
-- Definition for table entradas
--
DROP TABLE IF EXISTS entradas;
CREATE TABLE entradas (
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  texto VARCHAR(255) DEFAULT NULL,
  foto VARCHAR(60) DEFAULT NULL,
  foto_grande VARCHAR(60) DEFAULT NULL,
  titulo VARCHAR(255) NOT NULL,
  tipos VARCHAR(40) DEFAULT NULL,
  enlace VARCHAR(255) DEFAULT NULL,
  pagina VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 46
AVG_ROW_LENGTH = 364
CHARACTER SET utf8
COLLATE utf8_spanish2_ci;

-- 
-- Dumping data for table entradas
--
INSERT INTO entradas VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a condimentum lorem. In hac habitasse platea dictumst. Morbi nisi sem, laoreet a ligula id, posuere mollis massa. Vivamus quis placerat ante, sit amet varius sem. Quisque eu mauris rhoncus,', 'images/thumb.gif', 'images/thumb.gif', 'En la playa', 'Noticias', '', 'inicio'),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a condimentum lorem. In hac habitasse platea dictumst.', 'images/thumb.gif', 'images/thumb.gif', 'No haciendo nada', 'Noticias', '', 'inicio'),
(3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a condimentum lorem. In hac habitasse platea dictumst. Morbi nisi sem, laoreet a ligula id, posuere mollis massa. Vivamus quis placerat ante, sit amet varius sem. Quisque eu mauris rhoncus,', 'images/thumb.gif', 'images/thumb.gif', 'Con mi ordenador', 'Noticias', '', 'inicio'),
(4, 'INICIO', '', '', '', 'Menú', 'index.php', 'inicio'),
(5, 'MIS AFICIONES', '', '', '', 'Menú', 'mis_aficiones.php', 'inicio'),
(6, 'MIS PELÍCULAS', '', '', '', 'Menú', 'mis_peliculas.php', 'inicio'),
(7, 'MIS LIBROS', '', '', '', 'Menú', 'mis_libros.php', 'inicio'),
(8, 'MIS JUEGOS', '', '', '', 'Menú', 'mis_juegos.php', 'inicio'),
(9, 'En esta Web puedes entender los gustos y preferencias de cada uno de los alumnos de clase', 'images/pitch.jpg', 'images/pitch.jpg', 'LA WEB ACTUAL DE LA CLASE', 'Encabezado', '', 'inicio'),
(10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a condimentum lorem. In hac habitasse platea dictumst. Morbi nisi sem, laoreet a ligula id, posuere mollis massa. Vivamus quis placerat ante, sit amet varius sem. Quisque eu mauris rhoncus,', 'images/thumb.gif', 'images/thumb.gif', 'Tenis', 'Noticias', '', 'mis_aficiones'),
(11, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a condimentum lorem. In hac habitasse platea dictumst.', 'images/thumb.gif', 'images/thumb.gif', 'Senderismo', 'Noticias', '', 'mis_aficiones'),
(12, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a condimentum lorem. In hac habitasse platea dictumst. Morbi nisi sem, laoreet a ligula id, posuere mollis massa. Vivamus quis placerat ante, sit amet varius sem. Quisque eu mauris rhoncus,', 'images/thumb.gif', 'images/thumb.gif', 'Candy Crash', 'Noticias', '', 'mis_aficiones'),
(13, 'INICIO', '', '', '', 'Menú', 'index.php', 'mis_aficiones'),
(14, 'MIS AFICIONES', '', '', '', 'Menú', 'mis_aficiones.php', 'mis_aficiones'),
(15, 'MIS PELÍCULAS', '', '', '', 'Menú', 'mis_peliculas.php', 'mis_aficiones'),
(16, 'MIS LIBROS', '', '', '', 'Menú', 'mis_libros.php', 'mis_aficiones'),
(17, 'MIS JUEGOS', '', '', '', 'Menú', 'mis_juegos.php', 'mis_aficiones'),
(18, 'Estas son la aficines que me hacen sonreír cada mañana cuando recuerdo todo lo que he hecho', 'images/pitch.jpg', 'images/pitch.jpg', 'AFICIONES PARA TIEMPOS DIFÍCILES', 'Encabezado', '', 'mis_aficiones'),
(19, 'Blatty, el autor de la novela, explicó que el argumento se inspiró en un hecho verídico sobre el que empezó a trabajar cuando aún era estudiante universitario. El hecho fue un supuesto exorcismo ocurrido en 1949, del que informó The Washington Post. En el', 'images/thumb.gif', 'images/thumb.gif', 'El exorcista', 'Noticias', '', 'mis_peliculas'),
(20, 'Como agua para chocolate es una novela rosa escrita por Laura Esquivel, publicada en 1989, que trata acerca de la vida de una mujer (Tita), sus amoríos y la relación de esta con su familia, todo relacionado con la importancia de la cocina y las recetas tí', 'images/thumb.gif', 'images/thumb.gif', 'Como agua para chocolate', 'Noticias', '', 'mis_peliculas'),
(21, 'Se trata de una película coral de humor absurdo, con un guion surrealista repleto de situaciones de humor delirante en un pueblo de la Sierra de Albacete. Un joven ingeniero español que trabaja en la Universidad de Oklahoma regresa a España para disfrutar', 'images/thumb.gif', 'images/thumb.gif', 'Amanece que no es poco', 'Noticias', '', 'mis_peliculas'),
(22, 'INICIO', '', '', '', 'Menú', 'index.php', 'mis_peliculas'),
(23, 'MIS AFICIONES', '', '', '', 'Menú', 'mis_aficiones.php', 'mis_peliculas'),
(24, 'MIS PELÍCULAS', '', '', '', 'Menú', 'mis_peliculas.php', 'mis_peliculas'),
(25, 'MIS LIBROS', '', '', '', 'Menú', 'mis_libros.php', 'mis_peliculas'),
(26, 'MIS JUEGOS', '', '', '', 'Menú', 'mis_juegos.php', 'mis_peliculas'),
(27, 'Las películas de tu vida no son mis películas. Pero me divierte comprobar cómo te divierten', 'images/pitch.jpg', 'images/pitch.jpg', 'PELÍCULAS EN LA ESQUINA DEL PASILLO', 'Encabezado', '', 'mis_peliculas'),
(28, 'De entre los trescientos personajes que aparecen, apenas encontraremos representantes de las clases más acomodadas, y del mismo modo no tienen relevancia los pertenecientes a la clase obrera o a los sectores marginados. Lo que predomina es la clase media ', 'images/thumb.gif', 'images/thumb.gif', 'La colmena', 'Noticias', '', 'mis_libros'),
(29, 'Las ratas es una novela de Miguel Delibes, publicada por primera vez en enero de 1962 y que narra la vida en un pequeño pueblo castellano, apartado de cualquier capital y notablemente más atrasado. Es una denuncia social que muestra la mísera existencia e', 'images/thumb.gif', 'images/thumb.gif', 'Las ratas', 'Noticias', '', 'mis_libros'),
(30, 'Lo que el viento se llevó (en inglés: Gone with the Wind) es una novela escrita por Margaret Mitchell; es uno de los libros más vendidos de la historia, un clásico de la literatura de los Estados Unidos y es junto a su adaptación al cine uno de los mayore', 'images/thumb.gif', 'images/thumb.gif', 'Lo que el viento se llevó', 'Noticias', '', 'mis_libros'),
(31, 'INICIO', '', '', '', 'Menú', 'index.php', 'mis_libros'),
(32, 'MIS AFICIONES', '', '', '', 'Menú', 'mis_aficiones.php', 'mis_libros'),
(33, 'MIS PELÍCULAS', '', '', '', 'Menú', 'mis_peliculas.php', 'mis_libros'),
(34, 'MIS LIBROS', '', '', '', 'Menú', 'mis_libros.php', 'mis_libros'),
(35, 'MIS JUEGOS', '', '', '', 'Menú', 'mis_juegos.php', 'mis_libros'),
(36, 'Estos libros resumen mi vida con esta afición tan dolorosa', 'images/pitch.jpg', 'images/pitch.jpg', 'LOS LIBROS QUE DEBÍ LEER', 'Encabezado', '', 'mis_libros'),
(37, 'El parchís es un juego de mesa derivado del pachisi y similar al ludo, al parqués y al parcheesi. Es muy popular en España. Se juega con 1 dado y 4 fichas para cada uno de los jugadores (de dos a cuatro, aunque también hay tableros para 6 u 8 jugadores). ', 'images/thumb.gif', 'images/thumb.gif', 'El parchís', 'Noticias', '', 'mis_juegos'),
(38, 'El nombre es de origen árabe (truk o truch), y algunos lingüistas creen que es el origen etimológico de la palabra truco,8​ debido precisamente a los ardides que se emplean en este juego. "Con el significado de juego de naipes aparece ya citado en 1443".', 'images/thumb.gif', 'images/thumb.gif', 'La flor', 'Noticias', '', 'mis_juegos'),
(39, '¿De verdad que esto es un juego?', 'images/thumb.gif', 'images/thumb.gif', 'Juego de tronos', 'Noticias', '', 'mis_juegos'),
(40, 'INICIO', '', '', '', 'Menú', 'index.php', 'mis_juegos'),
(41, 'MIS AFICIONES', '', '', '', 'Menú', 'mis_aficiones.php', 'mis_juegos'),
(42, 'MIS PELÍCULAS', '', '', '', 'Menú', 'mis_peliculas.php', 'mis_juegos'),
(43, 'MIS LIBROS', '', '', '', 'Menú', 'mis_libros.php', 'mis_juegos'),
(44, 'MIS JUEGOS', '', '', '', 'Menú', 'mis_juegos.php', 'mis_juegos'),
(45, 'No me gusta jugar a las cartas, prefiero el juego del poder.', 'images/pitch.jpg', 'images/pitch.jpg', 'JUGAR O NO JUGAR ...', 'Encabezado', '', 'mis_juegos');

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;