//
//
//Vamos a sustituir las funciones anomimas por otras de escribir y operar
//
var global = {
    limpiar: false,
    operacion: null,
    resultado:0,

    escribir:function(e){
         if (this.limpiar) {
               this.display.value = "";
                this.limpiar = false;
            }
                this.display.value += e.target.value;
    }, 

    operar:function(e){
        if(!this.limpiar || this.operacion==e.target.value){
            switch (this.operacion) { 
                case '+':
                //Mi resultado tiene que ser lo que tiene ahora mas lo que tiene display
                this.resultado+=parseInt(this.display.value);
                this.display.value=this.resultado;
                break;

                case '-':
                //Mi resultado tiene que ser lo que tiene ahora mas lo que tiene display
                this.resultado-=parseInt(this.display.value);
                this.display.value=this.resultado;
                break;

                default:
                    
                    //Guardamos el primer numero para no perderlo
                    this.resultado=parseInt(this.display.value);
            }
                
            }
                /*Estas dos operaciones las hace en todos los casos por lo que las ponemos fuera*/
                this.operacion=e.target.value;
                //Si limpiar es true es porque no ha recibido un numero sino que es otra operacion
                //porque cuando recibe
               this.limpiar=true;

    }
};


    window.addEventListener("load", (e) => {
    //Esto seran array de elements
    global.operaciones = document.querySelectorAll(".operacion");
    global.numeros = document.querySelectorAll(".numero");
    //El display sera un objeto de tipo text (no quiero que sea un array)
    global.display = document.querySelector(".display");

    /*Necesito escuchadores para esos elementos*/

    for (var c = 0; c < global.operaciones.length; c++) {
        global.operaciones[c].addEventListener("click", function(event){
        	global.operar.call(global,event);

        });
    }

    for (var c = 0; c < global.numeros.length; c++) {
        global.numeros[c].addEventListener("click", function(event){
            global.escribir.call(global,event);
        });
    }
});