//Creo una variable global de tipo array asociativo
//
//Las demas variables globales las pondremos como miembro de esta
//
var global = {
    limpiar: false,
    operacion: null,
    resultado:0

};


window.addEventListener("load", (e) => {
    //Esto seran array de elements
    global.operaciones = document.querySelectorAll(".operacion");
    global.numeros = document.querySelectorAll(".numero");
    //El display sera un objeto de tipo text (no quiero que sea un array)
    global.display = document.querySelector(".display");

    /*Necesito escuchadores para esos elementos*/
    for (var c = 0; c < global.operaciones.length; c++) {
        global.operaciones[c].addEventListener("click", (e) => {
        	if(!global.limpiar || global.operacion==e.taget.value){//si limpiar es distinto de true o la operacion es la misma...
        		

            //Tendremos varias operaciones por lo que usaremos un switch
            //
            switch (global.operacion) { /*Evalua la operacion en este instante*/
                
                case '+':
                //Mi resultado tiene que ser lo que tiene ahora mas lo que tiene display
                global.resultado=parseInt(global.display.value);
                global.display.value=global.resultado;
                break;

                case '-':
                //Mi resultado tiene que ser lo que tiene ahora mas lo que tiene display
                global.resultado=parseInt(global.display.value);
                global.display.value=global.resultado;
                break;

                default:
                    
                    //Guardamos el primer numero para no perderlo
                    global.resultado=parseInt(global.display.value);
            }
            	
            }
            	/*Estas dos operaciones las hace en todos los casos por lo que las ponemos fuera*/
           		global.operaciones=e.target.value;
           		//Si limpiar es true es porque no ha recibido un numero sino que es otra operacion
           		//porque cuando recibe
                global.limpiar=true;

        });
    }

    for (var c = 0; c < global.numeros.length; c++) {
        global.numeros[c].addEventListener("click", (e) => {
            if (global.limpiar) {
                global.display.value = "";
                global.limpiar = false;
            }
            //Utilizamos el propio display como acumulador
            //
            //Concateno el evento con el display
            global.display.value += e.target.value;

        });
    }
});