var contador=0;
var inicio, ultimo, incremento;

window.addEventListener("load", function() {

	   //Asignamos el evento a los botones para incrementar o decrementar
    document.querySelector("#restar").addEventListener("click", restar);
    document.querySelector("#sumar").addEventListener("click", sumar);

});

function restar(){
	
	

	if (inicio==undefined){

		inicio=document.querySelector("#inicio").value*1;
		ultimo=document.querySelector("#final").value*1;
		incremento=document.querySelector("#incremento").value*1;
		contador=ultimo;
		};

	if (contador>inicio) {
		contador-=incremento;
		document.querySelector("#resultado").value=contador;
		};
	};


function sumar(){

	if (inicio==undefined){

		inicio=document.querySelector("#inicio").value*1;
		ultimo=document.querySelector("#final").value*1;
		incremento=document.querySelector("#incremento").value*1;
		contador=inicio;
		};

	if (contador<ultimo) {
		contador+=incremento;
		document.querySelector("#resultado").value=contador;
		};
	};
