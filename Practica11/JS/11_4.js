function disminuir(){
	var barra=document.querySelector("#barra");
	var ancho=parseInt(barra.style.width);
	ancho=ancho-5;
	if(ancho>=0){
		barra.style.width=ancho+"%";
	}
}

function aumentar(){
	var barra=document.querySelector("#barra");
	var ancho=parseInt(barra.style.width);
	ancho=ancho+5;
	if(ancho<=100){
		barra.style.width=ancho+"%";
	}
}

window.addEventListener("load",function(){
	var barra=document.querySelector("#barra");
	barra.style.width="0%";
	document.querySelector("#disminuir").addEventListener("click",disminuir);
	document.querySelector("#aumentar").addEventListener("click",aumentar);
		}); 


