/*Creamos una clase para las fotos*/

var Fotos=function(){
	//privadas
	var c, i;
	var sup=this;

	//publicas
	this.src;
	this.contador;

	//metodos publicos que usaremos para mover las imagenes
	//
	/*avanzando*/
	/*Mejor avanzamos por pagina de cuatro en cuatro
	this.avance=function(){
		if (this.contador<12) {
			this.contador++;
			this.colocar();
		};
	};
	this.retroceso=function(){
		if (this.contador!=0) {
			this.contador--;
			this.colocar();
		};
	};*/

	/*necesitamos pasar pagina de 4 en 4 miniaturas*/
	this.cuatro=function(){
		this.contador+=4;
		if (this.contador>8) {
			this.final();
		} ;
		this.colocar();
	};
	/*O retroceder de 4 en 4*/
	this.menosCuatro=function(){
		if (this.contador>4) {
			this.contador-=4;
		} else{
			this.primera();
		};
		this.colocar();
	};

	/*Para volver a la primera pagina*/
	this.primera=function(){
		this.contador=0;
		this.colocar();
	};

	/*Para volver a la ultima pagina*/
	this.final=function(){
		this.contador=8;
		this.colocar();
	};

	//metodos privados
	//
	/*colocar las miniaturas*/

	this.colocar=function(){
		var miniaturas=document.querySelectorAll("#minis img");
		for (i = 0; i < miniaturas.length; i++) {
			miniaturas[i].src="./imgs/mini/" + imagenes[i+this.contador].src;
	
		/*Ahora asignamos el evento click a las miniaturas*/

			miniaturas[i].addEventListener("click",grande);

		};
	};

	//*Esto es para mostrar la imagen grande*//
	function grande(){
		//Primero recogemos en una variable el src de la miniatura seleccionada
	c=event.target.src;
	c=c.replace("mini","grandes");
		
	//Y a continuacion la ponemos en su lugar del docu para que se vea
	//
	var fotog=document.querySelector("#fotos img");
	fotog.src=c;
	};
	
	// Funcion constructora donde inicializamos el contador a uno
	function Fotos(){
		sup.contador=1;
	};
	// Llamamos aqui a la  clase
	Fotos();
};


/* Precarga de imagenes */
var imagenes=new Array();
for (var j = 0; j < 12; j++) {
	/*Cada imagen es un objeto de tipo Fotos*/
	imagenes[j]=new Fotos();
	/*Le ponemos a cada uno de estos objetos su propiedad src*/
	imagenes[j].src=(j+1)+".jpg";
};

/* Cargamos aqui la Pagina*/
window.addEventListener("load",cargar);

/*Asignamos ahora eventos*/
function cargar(){
	/*instanciamos un objeto de tipo Fotos*/
	var misImagenes=new Fotos();
	/*colocamos las miniaturas*/
	misImagenes.colocar();
	
	/*asignamos eventos a los botones*/
	var inicio=document.querySelector("#controles #inicio").addEventListener("click",
		function(){
			misImagenes.primera();
		});
	var siguiente=document.querySelector("#controles #siguiente").addEventListener("click",
		function(){
			misImagenes.cuatro();
		});
	var anterior=document.querySelector("#controles #anterior").addEventListener("click",
		function(){
			misImagenes.menosCuatro();
		});
	var final=document.querySelector("#controles #final").addEventListener("click",
		function(){
			misImagenes.final();
		});
};