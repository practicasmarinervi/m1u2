// Hacemos una precarga de las imagenes y creamos primero esta variable
var laminas = new Array();
// Declaramos variables globales
var pantalla;
var eventos;
var numeroLamina;

window.addEventListener("load", cargar);


function cargar() {

    pantalla = document.querySelector(".foto"); //llegamos al div de las laminas donde iran las fotos
    eventos = document.querySelectorAll("li"); //llegamos a los elementos del menu para seleccionar cada foto

    for (var i = 0; i < eventos.length; i++) {

        eventos[i].addEventListener("click", pantallas); //Asignamos el evento clic al menu superior de las laminas
        laminas[i] = new Image();
        laminas[i].url = "url(./imgs/w" + (i + 1) + ".png)";
        }
         
}

function pantallas(event) {
  
    var pagina = event.target.innerHTML; //El contenido de la variable pagina sera cada una de nuestras pantallas
    var wally=document.querySelector(".foto #wally");  //Este es el div que tiene el recuadro para enmarcarlo

    for (var i = 1; i <= eventos.length; i++) {
        if (pagina == "Lamina" + i) {
            pantalla.style.backgroundImage = laminas[i - 1].url;
            numeroLamina=i;
            wally.style.display= "none";
            buscarw();

        }
         
    }
       
}

function buscarw() { //Posicion respecto a la ventana del navegador

    var pos = document.querySelector(".foto"); //este es el div de la foto donde va a estar wally
    
    pos.onclick = function(event) {
        var posx = event.offsetX;

        var posy = event.offsetY;
        
        switch (numeroLamina){
          case 1:
          if (((posx >= 503) && (posx <= 526)) && ((posy >= 326) && (posy <= 357))) {
            
            wally.style.top= "310px";  
            wally.style.left= "480px";
            wally.style.display= "block";
        }
          break
          case 2:
          if (((posx >= 260) && (posx <= 280)) && ((posy >= 150) && (posy <= 180))) {
            
            wally.style.top= "135px";  
            wally.style.left= "235px";
            wally.style.display= "block";
        }
        break
       //console.log("Left: " + posx + "px - Top: " + posy + "px");
         case 3:
         
          if (((posx >= 418) && (posx <= 438)) && ((posy >= 340) && (posy <= 370))) {
            
             wally.style.top= "330px";  
            wally.style.left= "396px";
            wally.style.display= "block";
        }
        break
         case 4:
         
          if (((posx >= 512) && (posx <= 530)) && ((posy >= 331) && (posy <= 360))) {
            
            wally.style.top= "320px";  
            wally.style.left= "490px";
            wally.style.display= "block";
        }
        break
       
         case 5:

          if (((posx >= 583) && (posx <= 588)) && ((posy >= 321) && (posy <= 346))) {
            
            wally.style.top= "300px";  
            wally.style.left= "555px";
            wally.style.display= "block";
        }
         break
          
       }

    }


}