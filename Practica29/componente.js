


var Componente=function(arg){
	/*variables privadas*/
	var contenedor="";
	var texto="";
	var titulo="";
	var origen="";
	var tipo=1;

			/*funcion privada*/

			function crearElementos(){

				/*Creo toda la estructura del popUp*/
				/*Se queda almacenada en div*/
				contenedor=document.createElement("div");
				var cabecera=document.createElement("header");
				var contenido=document.createElement("div");
				var boton=document.createElement("button");
				boton.type="button";
				boton.innerHTML="cerrar";
				contenedor.appendChild(cabecera);
				contenedor.appendChild(contenido);
				contenedor.appendChild(boton);
				contenedor.className="salida";
				cabecera.innerHTML=titulo;
				contenido.innerHTML=texto;
				
			}

			this.colocar=function(){
				document.querySelector("body").appendChild(contenedor);
				contenedor.style.opacity=1;
				contenedor.style.transform="perspective(100px) rotateX(20deg)";
			}

			this.eliminar=function(){
				document.querySelector("body").removeChild(contenedor);
			}

			this.activar=function(){
				document.querySelector(origen).addEventListener("click",() => {
				  this.colocar();
				  this.desactivar();

				});
			}

			this.desactivar=function(){
				document.querySelector("button").addEventListener("click",() => {
				  contenedor.style.transition="all 1ms";
				  contenedor.style.opacity=0;
				  contenedor.style.transform="rotate(0deg)";
				  contenedor.addEventListener("transitioned",()=>{
				  	contenedor.style.transition="all 2s";
				  })

				});

			}


			// Metodo constructor
			this.Componente=function(arg){
				texto=arg.texto || "ejemplo de clase";
				titulo=arg.titulo || "titulo";
				origen=arg.origen;
				tipo=arg.tipo;
				crearElementos(); //Metodo para crear privado
			}

			this.Componente(arg); // Le paso lo que le haya pasado al instanciar el objeto

			
};